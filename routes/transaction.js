module.exports = function(app) {
	var Transaction = require('../models/transaction.js');

	create = function(req, res) {
		var transaction = new Transaction({
			bid: req.query.bid
		});
		transaction.save(function(err) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: transaction}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAll = function(req, res) {
		Transaction.find()
			.populate("bid")
			.exec(function(err, transactions) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: transactions}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

	readAllByBuyer = function(req, res) {
		Transaction.find()
			.populate("bid")
			.exec(function(err, transactions) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: transactions}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};
	
  	readById = function(req, res) {
		Transaction.findById(req.query._id)
			.populate("bid")
			.exec(function(err, transaction) {
				if(!err) {
					if (transaction) {
						res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: transaction}) + ');');
					} else {
						res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
					}
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

  	update = function(req, res) {
		Transaction.findById(req.query._id, function(err, transaction) {
			if(!err) {
				if (transaction) {
					// bid not change
					transaction.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: transaction}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

	updateStatusInvoiceBuying = function(req, res) {
		Transaction.findById(req.query._id, function(err, transaction) {
			if(!err) {
				if (transaction) {
					transaction.statusInvoiceBuying = req.query.statusInvoiceBuying;
					transaction.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: transaction}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	updateStatusInvoicePayment = function(req, res) {
		Transaction.findById(req.query._id, function(err, transaction) {
			if(!err) {
				if (transaction) {
					transaction.statusInvoicePayment = req.query.statusInvoicePayment;
					transaction.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: transaction}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

 	remove = function(req, res) {
		Transaction.findById(req.query._id, function(err, transaction) {
			if(!err) {
				if (transaction) {
					transaction.remove(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, msg: 'REMOVED!' }) + ');');
						}
						else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					})
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

    app.get('/transaction/create', create);
  	app.get('/transaction/readAll', readAll);
	app.get('/transaction/readAllByBuyer', readAllByBuyer);
  	app.get('/transaction/readById', readById);
  	app.get('/transaction/update', update);
	app.get('/transaction/updateStatusInvoiceBuying', updateStatusInvoiceBuying);
	app.get('/transaction/updateStatusInvoicePayment', updateStatusInvoicePayment);
  	app.get('/transaction/remove', remove);

}