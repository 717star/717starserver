module.exports = function(app) {
	var Invoice = require('../models/invoice.js');

	create = function(req, res) {
		var invoice = new Invoice({
			payerImporter: {
				companyName: req.query.payerImporter.companyName,
				country: req.query.payerImporter.country,
				contact: {
					person: req.query.payerImporter.contact.person,
					email: req.query.payerImporter.contact.email,
					phone: req.query.payerImporter.contact.phone,
					cel: req.query.payerImporter.contact.cel
				}
			},
			invoiseId: req.query.invoiseId,
			totalAmount: req.query.totalAmount,
			issueDate: req.query.issueDate,
			expirationDate: req.query.expirationDate,
			sent: req.query.sent,
			billOfLadingDate: req.query.billOfLadingDate,
			incoterms: req.query.incoterms,
			authorizedByDebtor : req.query.authorizedByDebtor,
			askAdvance: req.query.askAdvance,
			askDiscountRate: req.query.askDiscountRate,
			// status by default in model
			// file: req.body.file, // TODO: files manager
			seller: req.query.seller
		});
		invoice.save(function(err) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: invoice}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAll = function(req, res) {
		Invoice.find()
			.populate("seller")
			.exec(function(err, invoices) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: invoices}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

	readAllToValidate = function(req, res) {
		// TODO: filters and status pending or in bidding
		Invoice.find()
			.populate("seller")
			.exec(function(err, invoices) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: invoices}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

	readAllToBid = function(req, res) {
		// TODO: filters and status in bidding
		/*console.log(req.query.seller);
		var query = {};
		if (req.query.seller) query.seller.companyName = req.query.seller;
		Invoice.find(query)*/
		Invoice.find()
			.populate("seller")
			.exec(function(err, invoices) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: invoices}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

	readAllToDeal = function(req, res) {
		// TODO: status pending or cancel
		Invoice.find({seller: req.query.seller})
			.populate("seller")
			.exec(function(err, invoices) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: invoices}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

  	readById = function(req, res) {
		Invoice.findById(req.query._id)
			.populate("seller")
			.exec(function(err, invoice) {
				if(!err) {
					if (invoice) {
						res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: invoice}) + ');');
					} else {
						res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
					}
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

  	update = function(req, res) {
		Invoice.findById(req.query._id, function(err, invoice) {
			if(!err) {
				if (invoice) {
					invoice.payerImporter.companyName = req.query.payerImporter.companyName;
					invoice.payerImporter.country = req.query.payerImporter.country;
					invoice.payerImporter.contact.person = req.query.payerImporter.contact.person;
					invoice.payerImporter.contact.email = req.query.payerImporter.contact.email;
					invoice.payerImporter.contact.phone = req.query.payerImporter.contact.phone;
					invoice.payerImporter.contact.cel = req.query.payerImporter.contact.cel;
					invoice.invoiseId = req.query.invoiseId;
					invoice.totalAmount = req.query.totalAmount;
					invoice.issueDate = req.query.issueDate;
					invoice.expirationDate = req.query.expirationDate;
					invoice.sent = req.query.sent;
					invoice.billOfLadingDate = req.query.billOfLadingDate;
					invoice.incoterms = req.query.incoterms;
					invoice.authorizedByDebtor = req.query.authorizedByDebtor;
					invoice.askAdvance = req.query.askAdvance;
					invoice.askDiscountRate = req.query.askDiscountRate;
					// status not update
					// invoice.file = req.body.file; // TODO: files manager
					// seller not update
					invoice.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: invoice}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

	updateAsk = function(req, res) {
		Invoice.findById(req.query._id, function(err, invoice) {
			if(!err) {
				if (invoice) {
					invoice.askAdvance = req.query.askAdvance;
					invoice.askDiscountRate = req.query.askDiscountRate;
					invoice.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: invoice}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

 	remove = function(req, res) {
		Invoice.findById(req.query._id, function(err, invoice) {
			if(!err) {
				if (invoice) {
					invoice.remove(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, msg: 'REMOVED!' }) + ');');
						}
						else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					})
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

	cancel = function(req, res) {
		Invoice.findById(req.query._id, function(err, invoice) {
			if(!err) {
				if (invoice) {
					invoice.status = "CANCEL";
					invoice.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: invoice}) + ');');
						} else {
							res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: err}) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	validate = function(req, res) {
		Invoice.findById(req.query._id, function(err, invoice) {
			if(!err) {
				if (invoice) {
					invoice.status = "IN BIDDING";
					invoice.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: invoice}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	invalidate = function(req, res) {
		Invoice.findById(req.query._id, function(err, invoice) {
			if(!err) {
				if (invoice) {
					invoice.status = "INVALIDATED";
					invoice.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: invoice}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

    app.get('/invoice/create', create);
  	app.get('/invoice/readAll', readAll);
	app.get('/invoice/readAllToDeal', readAllToDeal);
	app.get('/invoice/readAllToBid', readAllToBid);
	app.get('/invoice/readAllToValidate', readAllToValidate);
  	app.get('/invoice/readById', readById);
  	app.get('/invoice/update', update);
	app.get('/invoice/updateAsk', updateAsk);
	app.get('/invoice/cancel', cancel);
	app.get('/invoice/validate', validate);
	app.get('/invoice/invalidate', invalidate);
  	app.get('/invoice/remove', remove);

}