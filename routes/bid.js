module.exports = function(app) {
	var Bid = require('../models/bid.js');

	create = function(req, res) {
		var bid = new Bid({
			buyer: req.query.buyer,
			invoice: req.query.invoice,
			askAdvance: req.query.askAdvance,
			askDiscountRate: req.query.askDiscountRate
		});
		bid.save(function(err) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: bid}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAll = function(req, res) {
		Bid.find()
			.populate("buyer")
			.populate("invoice")
			.exec(function(err, bids) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: bids}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

	readAllByInvoice = function(req, res) {
		Bid.find({invoice: req.query.invoice})
			.populate("buyer")
			.populate("invoice")
			.exec(function(err, bids) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: bids}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

	readAllByBuyer = function(req, res) {
		Bid.find({buyer: req.query.buyer})
			.populate("buyer")
			.populate("invoice")
			.exec(function(err, bids) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: bids}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};
	
  	readById = function(req, res) {
		Bid.findById(req.query._id)
			.populate("buyer")
			.populate("invoice")
			.exec(function(err, bid) {
				if(!err) {
					if (bid) {
						res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: bid}) + ');');
					} else {
						res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
					}
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

  	update = function(req, res) {
		Bid.findById(req.query._id, function(err, bid) {
			if(!err) {
				if (bid) {
					// buyer and invoice not change
					bid.askAdvance = req.body.askAdvance;
					bid.askDiscountRate = req.body.askDiscountRate;
					invoice.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: bid}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

 	remove = function(req, res) {
		Bid.findById(req.query._id, function(err, bid) {
			if(!err) {
				if (bid) {
					bid.remove(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, msg: 'REMOVED!' }) + ');');
						}
						else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					})
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

    app.get('/bid/create', create);
  	app.get('/bid/readAll', readAll);
	app.get('/bid/readAllByInvoice', readAllByInvoice);
	app.get('/bid/readAllByBuyer', readAllByBuyer);
  	app.get('/bid/readById', readById);
  	app.get('/bid/update', update);
  	app.get('/bid/remove', remove);

}