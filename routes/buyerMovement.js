module.exports = function(app) {
	var BuyerMovement = require('../models/buyerMovement.js');

	create = function(req, res) {
		var buyerMovement = new BuyerMovement({
			buyer: req.query.buyer,
			amount: req.query.amount,
			movementType: req.query.movementType,
			status: req.query.movementType == 'WITHDRAWAL'?  'PENDING VALIDATION' : 'ACTIVE'
		});
		buyerMovement.save(function(err) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyerMovement}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAll = function(req, res) {
		BuyerMovement.find()
			.populate("buyer")
			.exec(function(err, buyerMovements) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyerMovements}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

	readAllByStatus = function(req, res) {
		BuyerMovement.find({status: req.query.status})
			.populate("buyer")
			.exec(function(err, buyerMovements) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyerMovements}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

	readAllByBuyer = function(req, res) {
		BuyerMovement.find({buyer: req.query.buyer})
			.populate("buyer")
			.exec(function(err, buyerMovements) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyerMovements}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

	readAllByBuyerAndStatus = function(req, res) {
		BuyerMovement.find({buyer: req.query.buyer, status: req.query.status})
			.populate("buyer")
			.exec(function(err, buyerMovements) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyerMovements}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

	readAllByTypeAndStatus = function(req, res) {
		BuyerMovement.find({movementType: req.query.movementType, status: req.query.status})
			.populate("buyer")
			.exec(function(err, buyerMovements) {
				if(!err) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyerMovements}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
	};

  	readById = function(req, res) {
		BuyerMovement.findById(req.query._id)
			.populate("buyer")
			.exec(function(err, buyerMovement) {
				if(!err) {
					if (buyerMovement) {
						res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: buyerMovement}) + ');');
					} else {
						res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
					}
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
				}
			});
  	};

  	update = function(req, res) {
		BuyerMovement.findById(req.query._id, function(err, buyerMovement) {
			if(!err) {
				if (buyerMovement) {
					buyerMovement.amount = req.body.amount
					// buyer, type and statusnot change here. Only amount.
					buyerMovement.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: buyerMovement}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

 	remove = function(req, res) {
		BuyerMovement.findById(req.query._id, function(err, buyerMovement) {
			if(!err) {
				if (buyerMovement) {
					buyerMovement.remove(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, msg: 'REMOVED!' }) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					})
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

	activate = function(req, res) {
		BuyerMovement.findById(req.query._id, function(err, buyerMovement) {
			if(!err) {
				if (buyerMovement) {
					buyerMovement.status = 'ACTIVE';
					buyerMovement.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status:200, obj: buyerMovement}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};

	desactivate = function(req, res) {
		BuyerMovement.findById(req.query._id, function(err, buyerMovement) {
			if(!err) {
				if (buyerMovement) {
					buyerMovement.status = 'INACTIVE';
					buyerMovement.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status:200, obj: buyerMovement}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};

    app.get('/buyerMovement/create', create);
  	app.get('/buyerMovement/readAll', readAll);
	app.get('/buyerMovement/readAllByStatus', readAllByStatus);
	app.get('/buyerMovement/readAllByBuyer', readAllByBuyer);
	app.get('/buyerMovement/readAllByBuyerAndStatus', readAllByBuyerAndStatus);
	app.get('/buyerMovement/readAllByTypeAndStatus', readAllByTypeAndStatus);
  	app.get('/buyerMovement/readById', readById);
  	app.get('/buyerMovement/update', update);
  	app.get('/buyerMovement/remove', remove);
	app.get('/buyerMovement/activate', activate);
	app.get('/buyerMovement/desactivate', desactivate);

}