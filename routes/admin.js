module.exports = function(app) {
	var Admin = require('../models/admin.js');

	create = function(req, res) {
		var admin = new Admin({
			email: req.query.email,
			password: req.query.password
		});
		admin.save(function(err) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: admin}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAll = function(req, res) {
		Admin.find(function(err, admins) {
  			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: admins}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
  			}
  		});
  	};

	readById = function(req, res) {
		Admin.findById(req.query.id, function(err, admin) {
			if(!err) {
				if (admin) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: admin}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readByEmailPassword = function(req, res) {
		Admin.find({email: req.query.email, password: req.query.password}, function(err, admins) {
  			if(!err) {
				var admin = admins[0];
				if (admin) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: admin}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
  			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
  			}
  		});
  	};

  	update = function(req, res) {
		Admin.findById(req.query._id, function(err, admin) {
			if(!err) {
				if (admin) {
					admin.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: admin}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
  		});
  	};

 	remove = function(req, res) {
		Admin.findById(req.query.id, function(err, admin) {
			if(!err) {
				if (admin) {
					admin.remove(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, msg: 'REMOVED!' }) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					})
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

	updatePassword = function(req, res) {
		Admin.find({email: req.query.email, password: req.query.actualPassword}, function(err, admins) {
			if(!err) {
				var admin = admins[0];
				if (admin) {
					admin.password = req.query.newPassword;
					admin.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: admin}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	resetPassword = function(req, res) {
		Admin.find({email: req.query.email}, function(err, admins) {
			if(!err) {
				var admin = admins[0];
				if (admin) {
					admin.password = require('crypto').randomBytes(8).toString('hex');
					admin.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status:200, obj: admin}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				}
				else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};

    app.get('/admin/create', create);
  	app.get('/admin/readAll', readAll);
  	app.get('/admin/readById', readById);
	app.get('/admin/readByEmailPassword', readByEmailPassword);
  	app.get('/admin/update', update);
  	app.get('/admin/remove', remove);
	app.get('/admin/updatePassword', updatePassword);
	app.get('/admin/resetPassword', resetPassword);

}