module.exports = function(app) {
	var Buyer = require('../models/buyer.js');

	create = function(req, res) {
		var buyer = new Buyer({
			name: req.query.name,
			personType: req.query.personType,
			personName: req.query.personName,
			legalAddress: req.query.legalAddress,
			phone: req.query.phone,
			email: req.query.email,
			password: req.query.password? req.query.password : require('crypto').randomBytes(8).toString('hex'),
			bankAccount: {
				routingNumber: req.query.bankAccount.routingNumber,
				accountNumber: req.query.bankAccount.accountNumber,
				bankInfoNotes: req.query.bankAccount.bankInfoNotes
			},
			active: true
		});
		buyer.save(function(err) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyer}) + ');'); 
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAll = function(req, res) {
		Buyer.find(function(err, buyers) {
  			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyers}) + ');');
  			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
  			}
  		});
  	};

	readById = function(req, res) {
		Buyer.findById(req.query._id, function(err, buyer) {
			if(!err) {
				if (buyer) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyer}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};
	
	readByEmail = function(req, res) {
		Buyer.find({email: req.query.email}, function(err, buyers) {
			if(!err) {
				var buyer = buyers[0];
				if (buyer) {
					res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: buyer}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readByEmailPassword = function(req, res) {
		Buyer.find({email: req.query.email, password: req.query.password, active: true}, function(err, buyers) {
			if(!err) {
				var buyer = buyers[0];
				if (buyer) {
					res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: buyer}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readByName = function(req, res) {
		Buyer.find({name: req.query.name}, function(err, buyers) {
			if(!err) {
				var buyer = buyers[0];
				if (buyer) {
					res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: buyer}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

  	update = function(req, res) {
		Buyer.findById(req.query._id, function(err, buyer) {
			if(!err) {
				if (buyer) {
					buyer.name = req.query.name;
					buyer.personType = req.query.personType;
					buyer.personName = req.query.personName;
					buyer.legalAddress = req.query.legalAddress;
					buyer.phone = req.query.phone;
					buyer.bankAccount.routingNumber = req.query.backAccount.routingNumber;
					buyer.bankAccount.accountNumber = req.query.backAccount.accountNumber;
					buyer.bankAccount.bankInfoNotes = req.query.backAccount.bankInfoNotes;
					buyer.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: buyer}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

 	remove = function(req, res) {
		Buyer.findById(req.query._id, function(err, buyer) {
			if(!err) {
				if (buyer) {
					buyer.remove(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, msg: 'REMOVED!' }) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					})
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

	updatePassword = function(req, res) {
		Buyer.find({email: req.query.email, password: req.query.actualPassword}, function(err, buyers) {
			if(!err) {
				var buyer = buyers[0];
				if (buyer) {
					buyer.password = req.query.newPassword;
					buyer.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: buyer}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	resetPassword = function(req, res) {
		Buyer.find({email: req.query.email, active: true}, function(err, buyers) {
			if(!err) {
				var buyer = buyers[0];
				if (buyer) {
					buyer.password = require('crypto').randomBytes(8).toString('hex');
					buyer.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status:200, obj: buyer}) + ');'); 
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};

	activate = function(req, res) {
		Buyer.findById(req.query._id, function(err, buyer) {
			if(!err) {
				if(buyer) {
					buyer.active = true;
					buyer.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status:200, obj: buyer}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};

	desactivate = function(req, res) {
		Buyer.findById(req.query._id, function(err, buyer) {
			if(!err) {
				if(buyer) {
					buyer.active = false;
					buyer.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status:200, obj: buyer}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};
	
    app.get('/buyer/create', create);
  	app.get('/buyer/readAll', readAll);
  	app.get('/buyer/readById', readById);
	app.get('/buyer/readByEmail', readByEmail);
	app.get('/buyer/readByEmailPassword', readByEmailPassword);
	app.get('/buyer/readByName', readByName);
  	app.get('/buyer/update', update);
  	app.get('/buyer/remove', remove);
	app.get('/buyer/updatePassword', updatePassword);
	app.get('/buyer/resetPassword', resetPassword);
	app.get('/buyer/activate', activate);
	app.get('/buyer/desactivate', desactivate);
}