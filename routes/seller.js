module.exports = function(app) {
	var Seller = require('../models/seller.js');

	create = function(req, res) {
		var seller = new Seller({
			companyName: req.query.companyName,
            activityIndustry: req.query.activityIndustry,
			country: req.query.country,
			contactFirstLastName: req.query.contactFirstLastName,
            phone: req.query.phone,
            email: req.query.email,
			password: req.query.password? req.query.password : require('crypto').randomBytes(8).toString('hex'),
			legalAddress: req.query.legalAddress,
            webSite: req.query.webSite,
			bankAccount: {
				routingNumber: req.query.bankAccount.routingNumber,
				swiftNumber: req.query.bankAccount.swiftNumber,
				accountNumber: req.query.bankAccount.accountNumber,
				bankInfoNotes: req.query.bankAccount.bankInfoNotes
			},
            active: true
		});
		seller.save(function(err) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: seller}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAll = function(req, res) {
		Seller.find(function(err, sellers) {
  			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: sellers}) + ');');
  			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
  			}
  		});
  	};

	readById = function(req, res) {
		Seller.findById(req.query._id, function(err, seller) {
			if(!err) {
				if (seller) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: seller}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readByEmail = function(req, res) {
		Seller.find({email: req.query.email}, function(err, sellers) {
			if(!err) {
				var seller = sellers[0];
				if (seller) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: seller}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readByEmailPassword = function(req, res) {
		Seller.find({email: req.query.email, password: req.query.password, active: true}, function(err, sellers) {
			if(!err) {
				var seller = sellers[0];
				if (seller) {
					res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: seller}) + ');');
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	readAllByIndustryCountry = function(req, res) {
		var query = {};
		if (req.query.industry) query.activityIndustry = req.query.industry;
		if (req.query.country) query.country = req.query.country;
		Seller.find(query, function(err, sellers) {
			if(!err) {
				res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: sellers}) + ');');
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

  	update = function(req, res) {
		Seller.findById(req.query._id, function(err, seller) {
			if(!err) {
				if (seller) {
					seller.companyName = req.query.companyName;
					seller.activityIndustry = req.query.activityIndustry;
					seller.country = req.query.country;
					seller.contactFirstLastName = req.query.contactFirstLastName;
					seller.phone = req.query.phone;
					seller.legalAddress = req.query.legalAddress;
					seller.webSite = req.query.webSite;
					seller.bankAccount.routingNumber = req.query.bankAccount.routingNumber;
					seller.bankAccount.swiftNumber = req.query.bankAccount.swiftNumber;
					seller.bankAccount.accountNumber = req.query.bankAccount.accountNumber;
					seller.bankAccount.bankInfoNotes = req.query.bankAccount.bankInfoNotes;
					seller.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, obj: seller}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			}
			else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};
	
 	remove = function(req, res) {
		Seller.findById(req.query._id, function(err, seller) {
			if(!err) {
				if (seller) {
					seller.remove(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status: 200, msg: 'REMOVED!' }) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					})
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
  		});
  	};

	updatePassword = function(req, res) {
		Seller.find({email: req.query.email, password: req.query.actualPassword}, function(err, sellers) {
			if(!err) {
				var seller = sellers[0];
				if (seller) {
					seller.password = req.query.newPassword;
					seller.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '(' + JSON.stringify({status: 200, obj: seller}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '(' + JSON.stringify({status: 500, msg: 'DO NOT EXISTS!'}) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status: 500, msg: err }) + ');');
			}
		});
	};

	resetPassword = function(req, res) {
		Seller.find({email: req.query.email, active: true}, function(err, sellers) {
			if(!err) {
				var seller = sellers[0];
				if (seller) {
					seller.password = require('crypto').randomBytes(8).toString('hex');
					seller.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status:200, obj: seller}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});	
				}
				else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};

	activate = function(req, res) {
		Seller.findById(req.query._id, function(err, seller) {
			if(!err) {
				if (seller) {
					seller.active = true; 
					seller.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status:200, obj: seller}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};

	desactivate = function(req, res) {
		Seller.findById(req.query._id, function(err, seller) {
			if(!err) {
				if (seller) {
					seller.active = false;
					seller.save(function (err) {
						if (!err) {
							res.send(req.query.callback + '('+ JSON.stringify({status:200, obj: seller}) + ');');
						} else {
							res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
						}
					});
				} else {
					res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: 'DO NOT EXISTS!' }) + ');');
				}
			} else {
				res.send(req.query.callback + '('+ JSON.stringify({status:500, msg: err }) + ');');
			}
		});
	};
	
	app.get('/seller/create', create);
  	app.get('/seller/readAll', readAll);
  	app.get('/seller/readById', readById);
	app.get('/seller/readByEmail', readByEmail);
	app.get('/seller/readByEmailPassword', readByEmailPassword);
	app.get('/seller/readAllByIndustryCountry', readAllByIndustryCountry);
  	app.get('/seller/update', update);
  	app.get('/seller/remove', remove);
	app.get('/seller/updatePassword', updatePassword);
	app.get('/seller/resetPassword', resetPassword);
	app.get('/seller/activate', activate);
	app.get('/seller/desactivate', desactivate);
}