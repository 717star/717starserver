var mongoose = require('mongoose');

var buyerMovementSchema = new mongoose.Schema({
	buyer:  {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Buyer" 
	},
	amount: { type: Number },
	movementType: {
		type: String,
		enum : ['WITHDRAWAL', 'DEPOSIT']
	},
	status: {
		type: String,
		enum : ['PENDING VALIDATION', 'ACTIVE', 'INACTIVE']
	}
});

module.exports = mongoose.model('BuyerMovement', buyerMovementSchema);