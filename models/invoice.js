var mongoose = require('mongoose');

var invoiceSchema = new mongoose.Schema({
	payerImporter: {
		companyName: { type: String },
		country: { type: String },
		contact : {
			person: { type: String },
			email: { type: String },
			phone: { type: String },
			cel: { type: String },
		}
	},
	invoiseId: { type: Number },
	totalAmount: { type: Number },
	issueDate: { type: Date },
	expirationDate: { type: Date },
	sent: { type: Boolean },
	billOfLadingDate: { type: Date },
	incoterms: { type: String },
	authorizedByDebtor: { type: Boolean },
	askAdvance: { type: Number },
	askDiscountRate: { type: Number },
	status: {
		type: String,
		enum : ['PENDING VALIDATION', 'INVALIDATED', 'IN BIDDING', 'CANCEL', 'SOLD', 'NO SOLD'],
		default : 'PENDING VALIDATION'
	},
	file: { type: String },
	seller:  {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Seller' 
	}
});

module.exports = mongoose.model('Invoice', invoiceSchema);