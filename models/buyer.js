var mongoose = require('mongoose');

var buyerSchema = new mongoose.Schema({
	name: { type: String },
	personType: { type: Boolean },
	personName: { type: String },
	legalAddress: { type: String },
	phone: { type: String },
	email: { type: String },
	password: { type: String },
	bankAccount: {
		routingNumber: { type: String },
		accountNumber: { type: String },
		bankInfoNotes: { type: String }
	},
	active: { type: Boolean }
});

module.exports = mongoose.model('Buyer', buyerSchema);