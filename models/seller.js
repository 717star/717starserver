var mongoose = require('mongoose');

var sellerSchema = new mongoose.Schema({
	companyName: { type: String },
	activityIndustry: { type: String },
	country: { type: String },
	contactFirstLastName: { type: String },
	phone: { type: String },
	email: { type: String },
	password: { type: String },
	legalAddress: { type: String },
	webSite: { type: String },
	bankAccount: {
		routingNumber: { type: String },
		swiftNumber: { type: String },
		accountNumber: { type: String },
		bankInfoNotes: { type: String }
	},
	active: { type: Boolean }
});

module.exports = mongoose.model('Seller', sellerSchema);