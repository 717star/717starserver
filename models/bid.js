var mongoose = require('mongoose');

var bidSchema = new mongoose.Schema({
	buyer:  {
		type: mongoose.Schema.Types.ObjectId, 
		ref: "Buyer" 
	},
	invoice:  {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Invoice" 
	},
	askAdvance: { type: Number },
	askDiscountRate: { type: Number }
});

module.exports = mongoose.model('Bid', bidSchema);