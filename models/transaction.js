var mongoose = require('mongoose');

var transactionSchema = new mongoose.Schema({
	bid:  {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Bid" 
	},
	statusInvoiceBuying: {
		type: String,
		enum : ['N/A', 'PENDING OF CONFIRMATION', 'PAID TO THE SELLER'],
		default : 'N/A'
	},
	statusInvoicePayment: {
		type: String,
		enum : ['N/A', 'PENDING OF PAYMENT', 'PAID', 'OVERDUE'],
		default : 'N/A'
	}
});

module.exports = mongoose.model('Transaction', transactionSchema);