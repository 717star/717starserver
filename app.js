// ***************SERVER****************
var express = require("express"),
    app = express(),
    http = require("http"),
    server = http.createServer(app),
    mongoose = require('mongoose'),
    mailer = require('express-mailer');

app.configure(function () {
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
});

routes = require('./routes/seller')(app);
routes = require('./routes/buyer')(app);
routes = require('./routes/admin')(app);
routes = require('./routes/invoice')(app);
routes = require('./routes/bid')(app);
routes = require('./routes/transaction')(app);
routes = require('./routes/buyerMovement')(app);

server.listen(3000, function() {
    console.log("Node server running on http://localhost:3000");
});
// ***************SERVER****************

// ***************DB****************
mongoose.connect('mongodb://localhost/717', function(err, res) {
    if(err) {
        console.log('ERROR: connecting to Database. ' + err);
    } else {
        console.log('Connected to Database');
    }
});
// ***************DB****************

// ***************MAILER****************
mailer.extend(app, {
    from: 'no-reply@example.com',
    host: 'smtp.gmail.com', // hostname
    secureConnection: true, // use SSL
    port: 465, // port for secure SMTP
    transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts
    auth: {
        user: '717startest@gmail.com',
        pass: '717startest123'
    }
});

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

app.get('/email/:email/:password', function (req, res) {
    app.mailer.send('email', {
        to: req.params.email,
        subject: 'Welcome to 717Star',
        password: req.params.password
    }, function (err) {
        if (err) {
            // handle error
            console.log(err);
            res.send('There was an error sending the email');
            return;
        }
        res.send('Email Sent');
    });
});
// ***************MAILER****************